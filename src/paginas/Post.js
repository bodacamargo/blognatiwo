import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import * as moment from 'moment';
import 'moment/locale/pt-br';
import blogApi from '../services/blogApi';
const blog = new blogApi();

export default class Post extends Component {
	constructor(props) {
		super(props);
		this.state = {
			category: [],
			categoryName: '',
			author: '',
			post: JSON.parse(sessionStorage.getItem('post')),
		};
	}

	async loadApi() {
		//recebe a categoria do post e author
		const category = await blog.load(`/categories?post=${this.state.post.id}`);
		const author = await blog.load(`/users/${this.state.post.author}`);

		this.setState({
			category,
			categoryName: category[0].name,
			author: author.name,
		});
	}

	componentDidMount() {
		this.loadApi();
	}

	render() {
		const { categoryName, author, post } = this.state;

		return (
			<main>
				{/* POST DESTAQUE (MAIS RECENTE) */}
				<section className="container esp-top">
					<div className="row post-principal-content">
						<article className="imagem-post" autoFocus>
							<h1>{post.title.rendered}</h1>
							<p className="autor-blog">
								Por <strong>{author}</strong> | {moment(post.date_gmt).format('LL')} | em{' '}
								<strong>{categoryName}</strong>{' '}
							</p>
							<img src={post._embedded['wp:featuredmedia']['0'].source_url} alt="Post principal" />
							<p dangerouslySetInnerHTML={{ __html: post.content.rendered }} />
						</article>
						<img className="sombra-post" src="img/row-bot.jpg" alt="Linha" />
					</div>
				</section>
				<nav className="paginacao">
					<ul className="pagination justify-content-center">
						<li className="page-item">
							<Link className="page-link" to="/blog" aria-label="Ant" name="prev">
								<span aria-hidden="true">&laquo; Ver mais postagens</span>
							</Link>
						</li>
					</ul>
				</nav>
			</main>
		);
	}
}
