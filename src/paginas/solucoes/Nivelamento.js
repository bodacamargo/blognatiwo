import React from 'react';
import {Link} from 'react-router-dom';

const Nivelamento = props => {
    return (
        <main>
            {/* NIVELAMENTO */}
            <section className='container'>
            	<div className='row solucao-img'>
            		{/* <img src='img/nivelamento.svg' alt='Logo Saraiva Educação | Nivelamento'/> */}
            	</div>
            	<div className='row'>
           		
            		{/* MENU LATERAL ESQUERDO */}
            		<nav className='col-md-3'>
            			<div className='list-group navegacao'>
						<Link to='./#nivelamento' className='list-group-item list-group-item-action'>O Nivelamento</Link>
						<Link to='./#beneficios' className='list-group-item list-group-item-action'>Benefícios Gerais</Link>
						<Link to='./#funcionalidades' className='list-group-item list-group-item-action'>Funcionalidades</Link>
						<Link to='./#depoimentos' className='list-group-item list-group-item-action'>Depoimentos</Link>
					</div>	
            		</nav>

            		
            		{/* CONTEUDO */}
            		<div className='col-md-9'>
            			<h3 className='subtitulos' id='nivelamento'>O Nivelamento</h3>
            			<div className='row icones'>
            				<div className='col-md-4'>
            					<img src='icons/trilhas.svg' alt=''/>
            					<p>Trilhas de revisão do conhecimento</p>
            				</div>
            				<div className='col-md-4'>
            					<img src='icons/relatorios.svg' alt=''/>
            					<p>Relatórios diagnósticos</p>
            				</div> 
            				<div className='col-md-4'>
            					<img src='icons/indicadores.svg' alt=''/>
            					<p>Atende a indicadores previstos no MEC</p>
            				</div>    				
            			</div>

						{/* TEXTO SOLUCAO */}
            			<div className='row texto'>
            				<p>Para muitas IES, o nível dos alunos ingressantes é uma questão que gera o dilema: manter alto o nível de exigência na captação e perder potenciais alunos ou utilizar um sistema menos  rigoroso e trabalhar para garantir que todos se desenvolvam para ter uma base sólida capaz de sustentar a graduação?</p>
            				<p>Nós, da Saraiva Educação, acreditamos que todas as pessoas merecem ter acesso a um nsino Superior de qualidade. Por isso, nossa escolha é trabalhar o potencial de cada estudante, ajudando-o a exercer sua capacidade máxima por meio do Nivelamento.</p> <p>O Nivelamento é uma solução de apoio aos alunos ingressantes no Ensino Superior. Nela, são utilizados exercícios, videoaulas e elementos de gamificação em formato de trilhas do conhecimento. Desse modo, a plataforma é capaz de diagnosticar, mapear e recuperar aprendizagens do Ensino Médio necessárias ao bom desenvolvimento do estudante na graduação, diminuindo os índices de evasão.</p>
            			</div>
						
						{/* BENEFÍCIOS */}
            			<div className='row' id='beneficios'>
            				<h3 className='subtitulos'>Benefícios Gerais</h3>
            				<ul className='lista'>
            					<li>Aumento do aproveitamento das disciplinas, refletindo no sucesso acadêmico do aluno;</li>
            					<li>Melhoria na captação de alunos: postura inovadora da IES aumenta o valor percebido pelos potenciais estudantes;</li>
            					<li>Redução da evasão de alunos, devido ao apoio oferecido pela IES.</li>
            				</ul>
            			</div>

            			{/* FUNCIONALIDADES */}
            			<div className='row' id='funcionalidades'>
            				<h3 className='subtitulos'>Principais Funcionalidades</h3>
            				<ul className='lista'>
            					<li>Banco de questões padrão Enem e dos principais vestibulares do país;</li>
            					<li>Trilhas gamificadas para revisão do conhecimento do Ensino Médio;</li>
            					<li>Trilhas personalizadas para os diferentes cursos, de modo a fornecer uma revisão mais eficiente;</li>
            					<li>Videoaulas para a retomada de conceitos vistos no Ensino Médio;</li>
            					<li>Relatórios para diagnóstico.</li>
            				</ul>
            			</div>

            			{/* DEPOIMENTOS */}
            			<div className='row' id='depoimentos'>
            				<h3 className='subtitulos'>Depoimentos</h3>
            				<ul className='lista'>
            					<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec quis euismod risus.</li>
            					<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec quis euismod risus.</li>
            				</ul>
            			</div>

            			{/* CALL TO ACTION */}
            			<div className='row call'>
            				<div className="col-md-7">
            					<h4 className='subtitulos'>Quer aumentar o desempenho de seus alunos no Exame da OAB?</h4>
            				</div>
            				<div className="col-md-5">
            					<Link className='btn botao' to='/contato'>Fale com um especialista</Link>            					
            				</div>
            			</div>						
            		</div>
            	</div>               
            </section>
        </main>        
    )
}
export default Nivelamento;