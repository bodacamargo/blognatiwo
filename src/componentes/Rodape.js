import React from 'react';
import {Link} from 'react-router-dom';

const Rodape = props => {
    return (
        <footer>
            <section className='container'>
                <div className='row'>
                    <div className='col-md-4 logo-footer'>
                        <img src='img/logo-saraiva-educacao-br.svg' alt='Logo Saraiva Educação'/>
                    </div>
                    <div className='col-md-4'>
                        <p className='email'>falecom@saraivaeducacao.com.br</p>
                        <p className='endereco'>Av. das Nações Unidas, 7221, 1º andar, Setor B<br />Pinheiros, São Paulo/SP - CEP: 05425-902</p>
                    </div>
                    <div className='col-md-4 social'>
                        <div className='row'>
                            <div className='col r'>
                                <Link to='//www.facebook.com/saraivaeducacao/'><img src='img/facebook.svg' alt='Facebook Saraiva Educação'/></Link>
                            </div>
                            <div className='col l'>
                                <Link to='//www.linkedin.com/company/saraiva-educacao/'><img src='img/linkedin.svg' alt='Linkedin Saraiva Educação'/></Link>
                            </div>
                        </div>
                    </div>
                </div>

                <div className='direitos'>
                    <p>Direitos Autorais © 2016 Somos Educação S.A. Todos os direitos reservados.</p>					
                </div>
            </section>
        </footer>
    )
}
export default Rodape;